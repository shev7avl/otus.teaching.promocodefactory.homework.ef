﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using SQLite;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(101)]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(50)]
        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public Guid RoleId { get; set; }

        //Navigation properties

        public virtual Role Role { get; set; }


    }
}
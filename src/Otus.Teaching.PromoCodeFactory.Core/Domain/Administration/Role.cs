﻿using System;
using SQLite;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
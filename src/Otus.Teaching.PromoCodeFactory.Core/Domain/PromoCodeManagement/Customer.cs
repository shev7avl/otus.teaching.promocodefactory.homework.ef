﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using SQLite;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(101)]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(50)]
        public string Email { get; set; }

        
        public virtual ICollection<CustomerPreference> Preferences { get; set; } 

        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}
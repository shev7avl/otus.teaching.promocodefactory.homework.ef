﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer CustomerFromRequest(CreateOrEditCustomerRequest request)
        {
            var id = Guid.NewGuid();
            var customer = new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            return customer;
        }

        public static Customer UpdateCustomer(Customer existingCustomer, CreateOrEditCustomerRequest newCustomer)
        {
            var id = existingCustomer.Id;
            if (existingCustomer.FirstName != newCustomer.FirstName) existingCustomer.FirstName = newCustomer.FirstName;
            if (existingCustomer.LastName != newCustomer.LastName) existingCustomer.LastName = newCustomer.LastName;       
            if (existingCustomer.Email != newCustomer.Email) existingCustomer.Email = newCustomer.Email;

            return existingCustomer;
        }
    }

}

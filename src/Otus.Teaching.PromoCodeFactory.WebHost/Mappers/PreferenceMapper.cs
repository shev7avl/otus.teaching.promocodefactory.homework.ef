﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PreferenceMapper
    {
        public static Preference PreferenceFromRequest(CreateOrEditPreferenceRequest request)
        {
            var id = Guid.NewGuid();
            var preference = new Preference
            {
                Id = id,
                Name = request.Name
            };

            return preference;
        }

        public static Preference UpdatePreference(Preference existingPreference, CreateOrEditPreferenceRequest request)
        {

            if (existingPreference.Name != request.Name) existingPreference.Name = request.Name;          

            return existingPreference;
        }
    }
}

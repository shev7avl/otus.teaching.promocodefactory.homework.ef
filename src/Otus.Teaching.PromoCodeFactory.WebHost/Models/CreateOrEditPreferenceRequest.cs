﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditPreferenceRequest
    {
        public string Name { get; set; }

    }
}
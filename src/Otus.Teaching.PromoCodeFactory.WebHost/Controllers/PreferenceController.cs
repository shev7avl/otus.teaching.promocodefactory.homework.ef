using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var response = preferences.Select(o => new PreferenceResponse(o)).ToList();

            return Ok(response);
        }
        /// <summary>
        /// Получить предпочтение по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            var response = new PreferenceResponse(preference);
            
            return Ok(response);
        }

        /// <summary>
        /// Создать новое предпочтение
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePreferenceAsync(CreateOrEditPreferenceRequest request)
        {
            Preference preference = PreferenceMapper.PreferenceFromRequest(request);

            await _preferenceRepository.AddEntityAsync(preference);

            return Ok();
        }
        /// <summary>
        /// Изменить предпочтения
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditPreferenceAsync(Guid Id, CreateOrEditPreferenceRequest request)
        {
            Preference existingPreference = await _preferenceRepository.GetByIdAsync(Id);
            existingPreference = PreferenceMapper.UpdatePreference(existingPreference, request);
            await _preferenceRepository.UpdateEntityAsync(existingPreference);

            return Ok();
        }
        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeletePreferenceAsync(Guid id)
        {
            Preference preference = await _preferenceRepository.GetByIdAsync(id);

            await _preferenceRepository.DeleteEntityAsync(preference);

            return Ok();
        }
    }
}
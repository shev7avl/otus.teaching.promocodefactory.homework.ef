﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        { 
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(o => new CustomerShortResponse(o)).ToList();

            return Ok(response);
        }
        /// <summary>
        /// Получить покупателя по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);

            return Ok(response);
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
        }
        
        /// <summary>
        /// Создать нового покупателя
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Customer customer = CustomerMapper.CustomerFromRequest(request);

            List<Preference> preferences = new List<Preference>();

            foreach (var item in request.PreferenceIds)
            {
                Preference pref = await _preferenceRepository.GetByIdAsync(item);
                preferences.Add(pref);
            }

            customer.Preferences = preferences.Select(o => new CustomerPreference
            {
                CustomerId = customer.Id,
                PreferenceId = o.Id
            }).ToList();

            await _customerRepository.AddEntityAsync(customer);

            return Ok();

        }
        /// <summary>
        /// Изменить данные покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var existingCustomer = await _customerRepository.GetByIdAsync(id);
            existingCustomer = CustomerMapper.UpdateCustomer(existingCustomer, request);

            List<Preference> preferences = new List<Preference>();

            foreach (var item in request.PreferenceIds)
            {
                Preference pref = await _preferenceRepository.GetByIdAsync(item);
                preferences.Add(pref);
            }

            existingCustomer.Preferences = preferences.Select(o => new CustomerPreference
            {
                CustomerId = existingCustomer.Id,
                PreferenceId = o.Id
            }).ToList();

            await _customerRepository.UpdateEntityAsync(existingCustomer);

            return Ok();
        }
        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            await _customerRepository.DeleteEntityAsync(customer);

            return Ok();
 
        }
    }
}
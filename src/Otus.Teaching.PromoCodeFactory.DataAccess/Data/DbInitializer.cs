﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer: IDbInitializer
    {
        private readonly DataContext _dataContext;

        public DbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            var employees = FakeDataFactory.Employees;
            
            var customers = FakeDataFactory.Customers;


            _dataContext.AddRange(employees);
            _dataContext.SaveChanges();

            _dataContext.AddRange(customers);
            _dataContext.SaveChanges();

        }
    }
}

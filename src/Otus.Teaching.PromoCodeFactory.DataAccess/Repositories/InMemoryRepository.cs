﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data;
        }
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data.ToList());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddEntityAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEntityAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteEntityAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}